# Exoplanet Microservice

This microservice manages exoplanets and provides functionalities to add, list, update, and delete exoplanets, as well as estimate fuel for a trip.

## Endpoints

- `POST /exoplanets`: Add a new exoplanet.
- `GET /exoplanets`: List all exoplanets.
- `GET /exoplanets/:id`: Get an exoplanet by ID.
- `PUT /exoplanets/:id`: Update an exoplanet.
- `DELETE /exoplanets/:id`: Delete an exoplanet.
- `GET /exoplanets/:id/fuel`: Get fuel estimation for a trip to an exoplanet and use crew_capacity(integer) as a query parameter to the number of crew members for whom fuel estimation is required.
## Running the Service

### Using Docker

1. Build the Docker image:

   docker build -t exoplanet-microservice .

2. Run the Docker container:

   docker run -p 8080:8080 exoplanet-microservice   


3. Testing the Service
### To run unit tests:

   go test   
