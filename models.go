package main

import "errors"

type ExoplanetType string

const (
	GasGiant    ExoplanetType = "GasGiant"
	Terrestrial ExoplanetType = "Terrestrial"
)

type Exoplanet struct {
	ID          int           `json:"id"`
	Name        string        `json:"name" binding:"required"`
	Description string        `json:"description" binding:"required"`
	Distance    int           `json:"distance" binding:"required,min=10,max=1000"`
	Radius      float64       `json:"radius" binding:"required,min=0.1,max=10"`
	Mass        float64       `json:"mass,omitempty" binding:"omitempty,min=0.1,max=10"`
	Type        ExoplanetType `json:"type" binding:"required,oneof=GasGiant Terrestrial"`
}

var (
	ErrInvalidType = errors.New("invalid exoplanet type")
)
