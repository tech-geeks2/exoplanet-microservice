package main

import "github.com/gin-gonic/gin"

func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.POST("/exoplanets", AddExoplanetHandler)
	r.GET("/exoplanets", ListExoplanetsHandler)
	r.GET("/exoplanets/:id", GetExoplanetByIDHandler)
	r.PUT("/exoplanets/:id", UpdateExoplanetHandler)
	r.DELETE("/exoplanets/:id", DeleteExoplanetHandler)
	r.GET("/exoplanets/:id/fuel", FuelEstimationHandler)

	return r
}
