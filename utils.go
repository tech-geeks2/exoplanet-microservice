package main

func CalculateFuel(e *Exoplanet, crewCapacity int) float64 {
	var gravity float64
	switch e.Type {
	case GasGiant:
		gravity = 0.5 / (e.Radius * e.Radius)
	case Terrestrial:
		gravity = e.Mass / (e.Radius * e.Radius)
	}
	return float64(e.Distance) / (gravity * gravity) * float64(crewCapacity)
}
