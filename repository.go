package main

import "sync"

var (
	exoplanets = make(map[int]*Exoplanet)
	nextID     = 1
	mu         sync.Mutex
)

func AddExoplanet(e *Exoplanet) int {
	mu.Lock()
	defer mu.Unlock()
	e.ID = nextID
	nextID++
	exoplanets[e.ID] = e
	return e.ID
}

func GetExoplanets() []*Exoplanet {
	mu.Lock()
	defer mu.Unlock()
	planets := make([]*Exoplanet, 0, len(exoplanets))
	for _, e := range exoplanets {
		planets = append(planets, e)
	}
	return planets
}

func GetExoplanetByID(id int) (*Exoplanet, bool) {
	mu.Lock()
	defer mu.Unlock()
	e, exists := exoplanets[id]
	return e, exists
}

func UpdateExoplanet(id int, e *Exoplanet) bool {
	mu.Lock()
	defer mu.Unlock()
	if _, exists := exoplanets[id]; !exists {
		return false
	}
	e.ID = id
	exoplanets[id] = e
	return true
}

func DeleteExoplanet(id int) bool {
	mu.Lock()
	defer mu.Unlock()
	if _, exists := exoplanets[id]; !exists {
		return false
	}
	delete(exoplanets, id)
	return true
}
