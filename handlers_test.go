package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddExoplanetHandler(t *testing.T) {
	router := SetupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/exoplanets", strings.NewReader(`{
		"name": "Test Planet",
		"description": "A test planet",
		"distance": 100,
		"radius": 1.0,
		"type": "GasGiant"
	}`))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)
}

func TestListExoplanetsHandler(t *testing.T) {
	router := SetupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/exoplanets", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestGetExoplanetByIDHandler(t *testing.T) {
	router := SetupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/exoplanets/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestUpdateExoplanetHandler(t *testing.T) {
	router := SetupRouter()

	// Add an exoplanet first
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/exoplanets", strings.NewReader(`{
		"name": "Test Planet",
		"description": "A test planet",
		"distance": 100,
		"radius": 1.0,
		"type": "GasGiant"
	}`))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusCreated, w.Code)

	// Update the exoplanet
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PUT", "/exoplanets/1", strings.NewReader(`{
		"name": "Updated Test Planet",
		"description": "An updated test planet",
		"distance": 100,
		"radius": 1.0,
		"type": "GasGiant"
	}`))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestDeleteExoplanetHandler(t *testing.T) {
	router := SetupRouter()

	// Add an exoplanet first
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/exoplanets", strings.NewReader(`{
		"name": "Test Planet",
		"description": "A test planet",
		"distance": 100,
		"radius": 1.0,
		"type": "GasGiant"
	}`))
	req.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(w, req)
	assert.Equal(t, http.StatusCreated, w.Code)

	// Delete the exoplanet
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("DELETE", "/exoplanets/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}
