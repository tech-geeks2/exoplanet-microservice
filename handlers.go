package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddExoplanetHandler(c *gin.Context) {
	var exoplanet Exoplanet
	if err := c.ShouldBindJSON(&exoplanet); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if exoplanet.Type == Terrestrial && exoplanet.Mass == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "mass is required for Terrestrial planets"})
		return
	}

	id := AddExoplanet(&exoplanet)
	c.JSON(http.StatusCreated, gin.H{"id": id})
}

func ListExoplanetsHandler(c *gin.Context) {
	planets := GetExoplanets()
	c.JSON(http.StatusOK, planets)
}

func GetExoplanetByIDHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid ID"})
		return
	}

	exoplanet, exists := GetExoplanetByID(id)
	if !exists {
		c.JSON(http.StatusNotFound, gin.H{"error": "exoplanet not found"})
		return
	}

	c.JSON(http.StatusOK, exoplanet)
}

func UpdateExoplanetHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid ID"})
		return
	}

	var exoplanet Exoplanet
	if err := c.ShouldBindJSON(&exoplanet); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if exoplanet.Type == Terrestrial && exoplanet.Mass == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "mass is required for Terrestrial planets"})
		return
	}

	if !UpdateExoplanet(id, &exoplanet) {
		c.JSON(http.StatusNotFound, gin.H{"error": "exoplanet not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "updated"})
}

func DeleteExoplanetHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid ID"})
		return
	}

	if !DeleteExoplanet(id) {
		c.JSON(http.StatusNotFound, gin.H{"error": "exoplanet not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "deleted"})
}

func FuelEstimationHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid exoplanet ID"})
		return
	}

	exoplanet, exists := exoplanets[id]
	if !exists {
		c.JSON(http.StatusNotFound, gin.H{"error": "Exoplanet not found"})
		return
	}

	crewCapacity, err := strconv.Atoi(c.Query("crew_capacity"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid crew capacity"})
		return
	}

	var gravity float64
	if exoplanet.Type == "GasGiant" {
		gravity = 0.5 / (exoplanet.Radius * exoplanet.Radius)
	} else {
		gravity = exoplanet.Mass / (exoplanet.Radius * exoplanet.Radius)
	}

	fuel := float64(exoplanet.Distance) / (gravity * gravity) * float64(crewCapacity)
	c.JSON(http.StatusOK, gin.H{"fuel_estimation": fuel})
}
